This is a set of PCBs for making joystick-to-USB adaptors using an Arduino Pro Micro board
and compatible with [MickGyver's sketches](https://github.com/MickGyver/DaemonBite-Retro-Controllers-USB) (or
[the MiSTer project's fork](https://github.com/MiSTer-devel/Retro-Controllers-USB-MiSTer)).

Not tested yet.

[View on CadLab](https://cadlab.io/projects/pro-micro-joystick-adaptors)
